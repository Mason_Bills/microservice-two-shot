from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Shoes, BinVO


# from django.forms.models import model_to_dict

# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request):
#     if request.method == "GET":
#         shoes = Shoes.objects.all()
#         return JsonResponse({"shoes": [model_to_dict(shoe) for shoe in shoes]}, safe=False)
#     elif request.method == "POST":
#         content = json.loads(request.body)
#         try:
#             bin = BinVO.objects.get(id=content["id"])
#             content["id"] = bin
#             del content["id"]  
#         except BinVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid bin id"},
#                 status=400,           

#             )
#         shoe = Shoes.objects.create(**content)
#         return JsonResponse(
#             model_to_dict(shoe),
#             safe=False,
#         )



# from django.forms.models import model_to_dict
import json


from common.json import ModelEncoder


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [ "manufacturer", "model_name", "color", "picture_url"]



class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["id"]

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        shoes_data = [ShoesListEncoder().default(shoe) for shoe in shoes]
        return JsonResponse({"shoes": shoes_data}, safe=False)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            bin_id = content["bin_war"]
            bin = BinVO.objects.get(id=bin_id)
            content["bin_war"] = bin.id
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin_war id"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        shoe.href = shoe.get_api_url()
        shoe.save()
        return JsonResponse(
            ShoesListEncoder().default(shoe),
            safe=False,
        )




# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request):
#     if request.method == "GET":
#         shoes = Shoes.objects.all()
#         return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder, safe=False)
#     elif request.method == "POST":
#         content = json.loads(request.body)
#         try:
#             bin_href = content["id"]
#             bin = BinVO.objects.get(id=bin_href)
#             content["id"] = bin
#         # try:
#         #     bin_href = BinVO.objects.get(id=content["bin_id"])
#         #     content["bin_id"] = bin_href
#         except BinVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid bin id"},
#                 status=400,           

#             )
#         shoe = Shoes.objects.create(**content)
#         return JsonResponse(
#             shoe,
#             encoder=ShoesListEncoder,
#             safe=False,
#         )

# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request):
#     if request.method == "GET":
#         shoes = Shoes.objects.all()
#         return JsonResponse(shoes, encoder=ShoesListEncoder, safe=False)
#     elif request.method == "POST":
#         data = json.loads(request.body)
#         bin_id = data.get("location_id")
#         wardrobe = BinVO.objects.get(id=bin_id)
#         new_shoe = Shoes(wardrobe=wardrobe)
#         new_shoe.save()
#         return JsonResponse(new_shoe, encoder=ShoesListEncoder, safe=False)
    
# require_http_methods(['GET', 'POST'])
# def api_list_shoes(request):
#     if request.method == "GET":
#         shoes = Shoes.objects.all()
#         shoes = list(shoes.values())  # convert QuerySet to list of dictionaries
#         return JsonResponse({"shoes": shoes})

#     else:
#         content = json.loads(request.body)

#         # Get the BinVO object and put it in the content dict
#         try:
#             bin_war = BinVO.objects.get(id=content["bin_war"])
#             content["bin_war"] = bin_war
#         except BinVO.DoesNotExist:
#             return JsonResponse({"message": "Invalid bin_war id"}, status=400)

#         shoes = Shoes.objects.create(**content)

#         # convert Shoes instance to dictionary
#         shoes = model_to_dict(shoes)

#         return JsonResponse(shoes, safe=False)