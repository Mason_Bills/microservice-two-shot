from django.db import models


# Create your models here.
from django.db import models

class BinVO(models.Model): 
    import_href = models.CharField(max_length=500)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=500)
    href = models.URLField(max_length=500)
    id = models.AutoField(primary_key=True)

    bin_war = models.ForeignKey(
        BinVO, 
        related_name='BinVO', 
        on_delete=models.CASCADE
        )