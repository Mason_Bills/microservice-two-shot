import django
import os
import sys
import time
import json
import requests
from shoes_rest.models import Shoes, BinVO


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Fetch JSON data from an API
            response = requests.get('http://example.com/api/shoes')
            data = response.json()
            
            for item in data:
                # Check if the bin_war exists
                try:
                    bin_war = BinVO.objects.get(import_href=item['bin_war'])
                except BinVO.DoesNotExist:
                    print('Invalid bin_war id', file=sys.stderr)
                    continue
                
                # Create new Shoes objects and save them to the database
                Shoes.objects.update_or_create(
                    href=item['href'],
                    defaults={
                        'manufacturer': item['manufacturer'],
                        'model_name': item['model_name'],
                        'color': item['color'],
                        'picture_url': item['picture_url'],
                        'bin_war': bin_war
                    }
                )
                
        except Exception as e:
            print(e, file=sys.stderr)
            
        time.sleep(60)


# def poll():
#     while True:
#         print('Shoes poller polling for data')
#         try:
#             # Write your polling logic, here
#             pass
#         except Exception as e:
#             print(e, file=sys.stderr)
#         time.sleep(60)


# if __name__ == "__main__":
#     poll()
